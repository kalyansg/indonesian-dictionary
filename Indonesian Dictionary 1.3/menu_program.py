import pickle
import pprint
from idlecolors import *
import os
import sys
from tabulate import tabulate


with open ('mypickle.pickle', 'rb') as default_dict:
    b_dict=pickle.load(default_dict)

printc( red('Practice from your English-Indonesian Vocabulary Dictionary. Only use small letter when using this program!'))
printc( red('This program is made with Python 3 and runs only on Python IDE Idle!'))
print ("=================================")

#print('Unsorted List - actual order of the dictionary:')
print(b_dict)

# Print the names of the columns.
print ("=================================")
entries = (len(b_dict.keys()))
n=entries-1
print ('Your present Dictionary is having ',entries , ' entries')
print('You can of course add more words using the update-program [Option - 6]')
print()


#print('Sorted List - Not the actual order of the dictionary:')
#pprint.pprint(b_dict)


score=0
i=0
flag01=0

## Text menu in Python
      
def menu():
    print ("=================================")

    print('Choose your option for the program: ')
    print("[1] Start your Indonesian Vocabulary practice here")
    print("[2] Continue to the next word - avoid this option when starting")
    print('[3] Translate an English Word [existing in Dictionary] to Indonesian')
    print('[4] Translate an Indonesian Word [existing in Dictionary] to English')
    print('[5] Print the tabulated English-Indonesian Dictionary')
    print('...need to double-click to see the Squeezed Tabulation')
    print('[6] Update Dictionary with new entries - [English - Indonesian] or Usage [Indonesian - Usage]')
    print('[7] Update Dictionary with multiple values of Indonesian words for an English word')
    print('[8] Delete an entry from the English-Indonesian Dictionary')
    print('[9] Print numbers 1-21 English-->Indonesian')
    print("[0] Exit the program. Show your score and where (index) you stopped.")
    print()
  
    
  
menu()

valid = False

while not valid: #loop until the user enters a valid int
    try:
        option = int(input('Enter your option 0-9: '))
        valid = True #if this point is reached, option is a valid int
    except ValueError:
        print('Please only input digits between 0 & 9')
    
while option != 0:
    if option == 1:
        
        flag01=1 # This flags that you have chosen this so when you choose option-2 (next) it would know the index to go to
        A='Enter the index, where you want to start - Choose a value [0-'
        B=']: '
        m=str(n)
        C=(A + m + B)
       
        while True:
            try:
                i = int(input(C))
                if 0 <= i <= n:
                    break
                raise ValueError()
            except ValueError:
                print("Wrong Input")
        
        starting_index=i
        keys_list = list(b_dict)
        key = keys_list[i]
        #print(key)
        value=(b_dict[key])
        #print(value)

        print('English word from dictionary: ',key)# Your starting point
        ans = input('Translate the above English word: ')# Indonesian translation
        if ans in b_dict.get(key):
            print('Good, correct translation!')
            score = score + 5
            i=i+1
            r=b_dict.get(ans, 'No usage available yet')
            print('Usage example: ',r)
        else:
            print('Wrong translation')
            print('The right translation is:')
            print(value)
            print('Choose again: ')
            i+i+1
            r=b_dict.get(ans, 'No usage available yet')
            print('Usage example: ',r)
            
    elif option == 2 and flag01==0:
        
        printc( red('First time round do not use option 2') )
        option=0

    elif option == 2:
            
        key = keys_list[i]
        print('English word from dictionary: ',key)# Your starting point
        ans = input('Translate the English word: ')# Indonesian translation
        if ans in b_dict.get(key):
            print('Good, correct translation!')
            print()
            score = score + 5
            i=i+1
            r=b_dict.get(ans, 'No usage available yet')
            print('Usage example: ',r)
        else:
            print('Wrong translation')
            print('The right translation is:')
            print(b_dict.get(key))
            print()
            print('Choose again: ')
            i+i+1
            r=b_dict.get(ans, 'No usage available yet')
            print('Usage example: ',r)
        

    elif option == 3:
         key1=input('Enter the English word you want to be translated to Indonesian: ')
         try:
          value1=(b_dict[key1])
          print(value1)
          r=b_dict.get(value1, 'No usage available yet')
          print('Usage example: ',r)
         except:
          print('The word not in the dictionary')
          print()
          pass

    elif option == 4:
         value2=input('Enter the Indonesian word you want to be translated to English: ')
         key2=([k for k,v in b_dict.items() if v == value2])

         if key2==[]:
             print('No such value existing in the present dictionary')
         else:
             print(key2)

    
    elif option == 5:
         exec(open("TabulatedEnglishIndonesianDictionary.py").read())

    
    elif option == 6:


        print('Update your Dictionary with new words or usage.')
        print ("=================================")

        # Print the names of the columns.
        print ("=================================")
        print ('Your present Dictionary is having ', (len(b_dict.keys())), ' words')
        pprint.pprint(b_dict)
        ans='y'

        while(ans=='y'):
             k=input('Enter key [new English word] or [Indonesian word for Usage]: ')
             v=input('Enter value [equivalent Indonesian word or Indonesian usage]: ')
             c_dict={k:v}
             print(c_dict)


             # c_dict.update(b_dict)
             # b_dict=c_dict

             b_dict.update(c_dict)
  
             ans=input('Any more update y/n: ')
             if (ans=='n'):
               break
             #print(c_dict)
             #print(b_dict)

        #d_dict = dict(sorted(b_dict.items(), key=lambda item: item[0]))

        # Print the names of the columns.
        print ("=================================")
        print ('Your new updated Dictionary has', (len(b_dict.keys())), ' words')
        print ('  ')
        pprint.pprint(b_dict)


        with open ('mypickle.pickle' , 'wb') as default_dict:
           pickle.dump(b_dict, default_dict)
   

    elif option == 7:

          print ("=================================")
          print ('Your present Dictionary')
          pprint.pprint(b_dict)

          print('Update your Dictionary with multiple values for a [Key].')
          k=input('Enter Key: ')
          x=input('Enter multiple values with ONE SPACE between values: ')


          v=x.split(' ')
          print(" ",v)

          d={k:v}
          print(d)

          b_dict.update(d)

          #print(b_dict)
          pprint.pprint(b_dict)

          with open ('mypickle.pickle' , 'wb') as default_dict:
            pickle.dump(b_dict, default_dict)

    elif option == 8:

          print ('Your present Dictionary is having ', (len(b_dict.keys())), ' words')
          pp = pprint.PrettyPrinter(indent=4)

          pp.pprint(b_dict)

          print('Update your Dictionary by deleting wrong key-value pair.')
          print ("=================================")


          remove_key=input('Enter key to remove: ')

          #if remove_key in b_dict:
          #del b_dict[remove_key]


          b_dict.pop(remove_key)

          pp.pprint(b_dict)

          with open ('mypickle.pickle' , 'wb') as default_dict:
            pickle.dump(b_dict, default_dict)

    elif option == 9:

          ind_numbers = ['one','two', 'three', 'four','five', 'six', 'seven', 'eight', 'nine']
          ind_numbers_dict = {key: b_dict[key] for key in ind_numbers}

          ind_numbers_dict['1-One']=ind_numbers_dict['one']
          del ind_numbers_dict['one']

          ind_numbers_dict['2-Two']=ind_numbers_dict['two']
          del ind_numbers_dict['two']

          ind_numbers_dict['3-Three']=ind_numbers_dict['three']
          del ind_numbers_dict['three']

          ind_numbers_dict['4-Four']=ind_numbers_dict['four']
          del ind_numbers_dict['four']

          ind_numbers_dict['5-Five']=ind_numbers_dict['five']
          del ind_numbers_dict['five']

          ind_numbers_dict['6-Six']=ind_numbers_dict['six']
          del ind_numbers_dict['six']

          ind_numbers_dict['7-Seven']=ind_numbers_dict['seven']
          del ind_numbers_dict['seven']

          ind_numbers_dict['8-Eight']=ind_numbers_dict['eight']
          del ind_numbers_dict['eight']

          ind_numbers_dict['9-Nine']=ind_numbers_dict['nine']
          del ind_numbers_dict['nine']
          
          d=ind_numbers_dict
          sdt=sorted(d)# sorted key
          sd={key:d[key] for key in sdt}# sorted dictionary

          print ('The present Number-Dictionary-1 is having ', (len(sd.keys())), ' numbers')
          headers = ["ENGLISH", "INDONESIAN"]
          print(tabulate(sd.items(), headers = headers, tablefmt = 'grid'))

          ind_numbers_1 = ['ten','eleven','twelve','thirteen','fourteen', 'fifteen', 'sixteen','seventeen','eighteen','nineteen','twenty','twentyone']
          ind_numbers_dict_1 = {key: b_dict[key] for key in ind_numbers_1}

          ind_numbers_dict_1['10-Ten']=ind_numbers_dict_1['ten']
          del ind_numbers_dict_1['ten']

          ind_numbers_dict_1['11-Eleven']=ind_numbers_dict_1['eleven']
          del ind_numbers_dict_1['eleven']

          ind_numbers_dict_1['12-Twelve']=ind_numbers_dict_1['twelve']
          del ind_numbers_dict_1['twelve']

          ind_numbers_dict_1['13-Thirteen']=ind_numbers_dict_1['thirteen']
          del ind_numbers_dict_1['thirteen']

          ind_numbers_dict_1['14-Fourteen']=ind_numbers_dict_1['fourteen']
          del ind_numbers_dict_1['fourteen']

          ind_numbers_dict_1['15-Fifteen']=ind_numbers_dict_1['fifteen']
          del ind_numbers_dict_1['fifteen']

          ind_numbers_dict_1['16-Sixteen']=ind_numbers_dict_1['sixteen']
          del ind_numbers_dict_1['sixteen']

          ind_numbers_dict_1['17-Seventeen']=ind_numbers_dict_1['seventeen']
          del ind_numbers_dict_1['seventeen']

          ind_numbers_dict_1['18-Eighteen']=ind_numbers_dict_1['eighteen']
          del ind_numbers_dict_1['eighteen']

          ind_numbers_dict_1['19-Nineteen']=ind_numbers_dict_1['nineteen']
          del ind_numbers_dict_1['nineteen']

          ind_numbers_dict_1['20-Twenty']=ind_numbers_dict_1['twenty']
          del ind_numbers_dict_1['twenty']

          ind_numbers_dict_1['21-Twentyone']=ind_numbers_dict_1['twentyone']
          del ind_numbers_dict_1['twentyone']

          d=ind_numbers_dict_1
          sdt=sorted(d)# sorted key
          sd={key:d[key] for key in sdt}# sorted dictionary

          print ('The present Number-Dictionary-2 is having ', (len(sd.keys())), ' numbers')
          headers = ["ENGLISH", "INDONESIAN"]
          print(tabulate(sd.items(), headers = headers, tablefmt = 'grid'))

    else:
        print("Invalid option. Choose again!")
     
    menu()                                
    option = int(input("Enter your option: "))

if flag01==0:
    print('Goodbye')
    sys.exit()
    
max_score=(i-starting_index)*5
percent_score=(score/max_score)*100
percent_score=str(round(percent_score, 2))

print('Your Score is: ', score, ' out of the maximum score ',max_score, ' in percentage = ',percent_score)

if i==0:
   print('You have not started the index. So, start at index 0 next time! Goodbye!')
else:
   print('You stopped at index ',i-1,' Start at ',i,' next time.')
   print("Goodbye!")


   

# ENGLISH to INDONESIAN
# Dictionary  with Learning Utilities & Usage Entries Using the Same Python Dictionary Structure
## <p style="text-align: center;">in Python3</p>
## <p style="text-align: center;">Version 1.3</p>
This version 1.3 is a significant upgrade with 208 entries. Now the menu program can access everything. Additionally, the same dictionary structure is used for storing 'usages'. New usages can also be added by the user.

 Many bugs have been removed. If some bugs are found do let me know. I shall try and fix it. Happy learning!

One may download these programs, and use, modify and distribute them as one pleases.

This program is written in Python 3. The program will be useful for those interested in learning how to develop a Python 3 Thursday, 28. April 2022 06:07AM 
Dictionary. The dictionary is in English -->> Indonesian format. The program can be used by those who are learning Indonesian at beginners level. The Learning Dictionary can complement learning Bhasa Indonesia through the popular language learning program [Duolingo](https://www.duolingo.com/learn) .

The following table shows an example of the Python 3 dictionary structure including the usage, which is put in the same structure.

| English/Indonesian | Indonesian/Usage |Comments  |
|--|--|--|
|  book| buku | book-key, buku-value |
| i |  saya  aku|i-key, saya-value1, aku-value2  |
| nine |sembilan  | nine-key, sembilan-value |
| price | harga |price-key, harga-value |
|buku  |saya suka buku -  [i love books]  |buku usage  |


##### You only need to use the menu_program.py to access everything in this Multi-featured Learning Dictionary.

#### NOTES:
1. All files should be put in the same directory.
2. Readme - which is this file giving you the explanations.
3. You need Python 3 program and Python Idle to run the Menu Program.
4. mypickle.pikle - folder containing the binary dictionary file (only machine readable). 
5. The present version 1.3 is populated with 208 entries. 
6. This program can be accessed by the accompanying menu_program.py, which drives all the utilities.
7.  The following options are availble in the menu program as shown below:


**Choose your option for the program: **

[1] Start your Indonesian Vocabulary practice here

[2] Continue to the next word - avoid this option when starting

[3] Translate an English Word [existing in Dictionary] to Indonesian

[4] Translate an Indonesian Word [existing in Dictionary] to English

[5] View the tabulated English-Indonesian Dictionary
...need to double-click to see the Squeezed Tabulation

[6] Update Dictionary with new entries - [English - Indonesian] or Usage [Indonesian - Usage]

[7] Update Dictionary with multiple values of Indonesian words for an English word

[8] Delete an entry from the English-Indonesian Dictionary

[9] Print numbers 1-21 English-->Indonesian

[0] Exit the program. Show your score and where (index) you stopped.

**Enter your option: **

#### The program has following learning points in Python:

How to serialize and de-serialize a Python dictionary using 'pickle' - which is storage and retrieval of machine-readable data? 

How to create a menu in Python and accept user inputs with error mqnagement?

How to use while-loop to branch actions as per user options?

How to use a flag to detect the path of the program flow?

How to make a quiz from a large python dictionary and keep track of user's progress?

How to provide translation of English words as per user's requirements?

How to provide a tabulated dictionary for scrolling through.

How to provide facility for updating data in a python dictionary?

How to delete a dictionary entry?

How to provide 'multiple values' for a 'key' in a python dictionary?

How to show specific key-value pairs and present them in a table?

How to keep a score for a user while doing the vocabulary quiz?

**List of Files**

>TabulatedEnglishIndonesianDictionary.py - Program for tabulated output

>idlecolors.py - Program for coloured outputs [only works on Idle IDE]

>menu_program.py - Main program for accessing the Learning Dictionary 

>mypickle.pickle - File storing the dictionary data in machine-readable format also includes the usage entries

>README.md - this file

All the above files can be accessed here [Link to Files](https://gitlab.com/kalyansg/indonesian-dictionary/-/tree/main/Indonesian%20Dictionary%201.3) 

If you have any question, please write to me at kalyan.chatterjea@yahoo.com.sg

Thanks for using these programs.

Kalyan Chatterjea

Singapore

Thursday, 28. April 2022 06:07AM 

 

 
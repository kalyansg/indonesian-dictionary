#English -->> Indonesian Dictionary in tabulated form
#All English words in small letters to help with python-sorting routine
#Right click to open the table
#Make sure you have pip utility installed
#Run this in the Terminal 'pip install tabulate'

from tabulate import tabulate
import pickle

with open ('mypickle.pickle', 'rb') as default_dict:
    d=pickle.load(default_dict)

sdt=sorted(d)# sorted key
sd={key:d[key] for key in sdt}# sorted dictionary

print ('The present Dictionary is having ', (len(sd.keys())), ' words')
headers = ["ENGLISH", "INDONESIAN"]
print(tabulate(sd.items(), headers = headers, tablefmt = 'grid'))
